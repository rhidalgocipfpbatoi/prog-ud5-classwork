
package prog.ud5.classwork;

/**
 *
 * @author batoi
 */
public class Activitat4 {
    
    static final int NUM_NUMEROS = 200;
    static final int MAX_NUMERO_GENERADO = 300;
    
    public static void main(String[] args) {
        
        int[]numeros = new int[NUM_NUMEROS];
        
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = (int)(Math.random()* MAX_NUMERO_GENERADO + 1);            
        }
        
        int suma = 0;
        for (int numero : numeros) {
            if (numero % 2 == 1) {
                suma += numero;
            }
        }
        
        System.out.printf("La suma total es %d%n", suma);
        
    }
}
