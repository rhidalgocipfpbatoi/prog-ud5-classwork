/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud5.classwork;
import java.util.Arrays;

/**
 *
 * @author batoi
 */
public class Activitat11 {
    
    public static void main(String[] args) {
        String[] nombres = {"Tatiana", "Daniel", "Alex", "Miguel Ángel"};
        
        System.out.println("Sin ordenar");
        System.out.println(Arrays.toString(nombres));
        
        ordenarPorSeleccion(nombres);
        
        System.out.println("Ordenados");
        System.out.println(Arrays.toString(nombres));
        
    }
                
    public static void ordenarPorSeleccion(String[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int indiceElementoMenor = i;
            
            for (int j = i + 1; j < array.length; j++) {
                if (array[j].compareTo(array[indiceElementoMenor]) < 0) {
                    indiceElementoMenor = j;
                }
            }
            
            if (indiceElementoMenor != i) {
                String aux = array[indiceElementoMenor];
                array[indiceElementoMenor] = array[i];
                array[i] = aux;
            }
        }
    }
}
