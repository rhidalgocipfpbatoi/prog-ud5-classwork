/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud5.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat2 {
    static final int TAMANYO_ARRAY = 10;
    static Scanner scanner;
    
    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        String[] textos = new String[TAMANYO_ARRAY];
        
        for (int i = 0; i < textos.length; i++) {
            System.out.print("Introduce valor " + (i+1) + ":");
            textos[i] = scanner.next();
        }
        
        for (int i = 0; i < textos.length; i+=2) {
            System.out.print(textos[i] + " ");
        }
    }
}
