/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud5.classwork;

/**
 *
 * @author batoi
 */
public class Activitat16 {
    
    public static void main(String[] args) {
        int[][] matriz = {
            {4, 6, 2, 6, 8, 9, 5},
            {2, 4, 6, 4, 1, 4, 7},
            {3, 5, 8, 2, 1, 3, 6},
            {3, 7, 9, 0, 1, 2, 1},
            {45, 21, 53, 6, 12, 12, 1}
        };
        // Punto 1
        mostrarMatriz(matriz);
        System.out.println("");
        
        // Punto 2
        mostrarFila(matriz, 4);
        System.out.println("");
        
        // Punto 3
        mostrarCelda(matriz, 3, 2);
        mostrarCelda(matriz, 4, 2);
        System.out.println("");
        intercambiarCeldas(matriz, 3, 2, 4, 2);
        mostrarMatriz(matriz);
        System.out.println("");

        // Punto 4
        mostrarColumna(matriz, 0);
        mostrarColumna(matriz, 3);
        System.out.println("");
        intercambiarColumnas(matriz, 0, 3);
        mostrarMatriz(matriz);
        System.out.println("");
    }
    
    public static void mostrarMatriz(int[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            mostrarFila(matriz, i);
        }
    }
    
    public static void mostrarFila(int[][]matriz, int fila) {
        for (int i = 0; i < matriz[fila].length; i++) {
            mostrarCelda(matriz, fila, i);
        }
        System.out.println("");            
    }
    
    public static void mostrarCelda(int[][]matriz, int fila, int columna){
        System.out.print(matriz[fila][columna] + " ");
    }
    
    public static void intercambiarCeldas(int[][]matriz, 
            int fila1, int columna1, int fila2, int columna2) {
        
        int aux = matriz[fila1][columna1];
        matriz[fila1][columna1] = matriz[fila2][columna2];
        matriz[fila2][columna2] = aux;
    }
    
    public static void intercambiarColumnas(int[][]matriz,
            int columna1, int columna2) {
        
        for (int i = 0; i < matriz.length; i++) {
            intercambiarCeldas(matriz, i, columna1, i, columna2);
        }
    }
    
    public static void mostrarColumna(int[][]matriz, int columna) {
        for (int i = 0; i < matriz.length; i++) {
            mostrarCelda(matriz, i, columna);
        }
        System.out.println("");
    }
    
}
