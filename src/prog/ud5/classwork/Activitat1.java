/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud5.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat1 {
    
    static final int TAMANYO_ARRAY = 10;
    static Scanner scanner;
    
    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        int[] numeros = new int[TAMANYO_ARRAY];
        
        for (int i = 0; i < numeros.length; i++) {
            System.out.print("Introduce número " + (i+1) + ":");
            numeros[i] = scanner.nextInt();
        }
        
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i] + " ");
        }
    }
}
