/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud5.classwork;

import java.util.Arrays;

/**
 *
 * @author batoi
 */
public class Activitat7 {
    
    public static void main(String[] args) {
        //Punto 1
        String[] diasDeLaSemana = {
                "dilluns", "dimarts",
                "dimecres", "dijous",
                "divendres", "dissabte",
                "diumenge"};
        
        // Punto 2
        String[] diasDeLaSemana2 = diasDeLaSemana;

        // Punto 3
        diasDeLaSemana2[0] = "Monday";
        System.out.println(Arrays.toString(diasDeLaSemana));
        System.out.println(Arrays.toString(diasDeLaSemana2));

        // Punto 4
        String[]diasDeLaSemana3 = copiaArray(diasDeLaSemana2);
        diasDeLaSemana3[0] = "Lunes";
        System.out.println(Arrays.toString(diasDeLaSemana));
        System.out.println(Arrays.toString(diasDeLaSemana2));
        System.out.println(Arrays.toString(diasDeLaSemana3));
    }
    
    public static String[] copiaArray(String []array) {
        
        String[] arrayCopia = new String[array.length];
        
        for (int i = 0; i < array.length; i++) {
            arrayCopia[i] = array[i];
        }
        
        return arrayCopia;
        
    }
}
