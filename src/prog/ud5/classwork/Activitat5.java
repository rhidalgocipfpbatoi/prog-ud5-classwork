/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud5.classwork;

import static prog.ud5.classwork.Activitat4.MAX_NUMERO_GENERADO;

/**
 *
 * @author batoi
 */
public class Activitat5 {
    
    static final int MAX_NUMERO_GENERADO = 9;
    
    public static void main(String[] args) {
        int[] numeros = new int[20];
        
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = (int)(Math.random()* MAX_NUMERO_GENERADO + 1);
        }
        
        for (int i = 1; i <= numeros.length; i++) {
            System.out.print(numeros[i-1]);
            
            if (i % 4 == 0) {
                System.out.print(" ");
            }
        }
        
        System.out.println("");
    }
    
}
