/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud5.classwork;

import java.util.Arrays;

/**
 *
 * @author batoi
 */
public class Activitat10 {
    
    public static void main(String[] args) {
        int[] array1 = Activitat6.crearArray();
        int[] array2 = Activitat6.crearArray();
        
        System.out.println("Sin ordenar");
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
        
        ordenarPorIntercambio(array1);
        ordenarPorIntercambio(array2);
        
        System.out.println("Ordenados");
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }
    
    public static void ordenarPorIntercambio(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[i]) {
                    int aux = array[j]; // Intercambio de datos
                    array[j] = array[i];
                    array[i] = aux;
                }
            }
        }
    } 
}
