
package prog.ud5.classwork;


/**
 *
 * @author batoi
 */
public class Activitat6 {
    
    static final int MAX_NUMERO_GENERADO = 50;

    public static void main(String[] args) {
        int[] numeros = crearArray();
        visualitzarArray(numeros);
        
        int indice = cercarZero(numeros);
        if (indice == -1) {
            System.out.println("No hay ceros");
        } else {
            System.out.println("Hay un cero en la posición " + indice);
        }
        
        intercanvia(numeros);
        visualitzarArray(numeros);
    }
    
    public static int[] crearArray() {
        int[] numeros = new int[10];
        
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = (int)(Math.random()* MAX_NUMERO_GENERADO + 1);
        }
        
        return numeros;
    }
    
    public static void visualitzarArray(int[] vector) {
        for (int numero : vector) {
            System.out.print(numero + " ");
        }
    }
    
    public static int cercarZero(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] == 0) {
                return i;
            }
        }
        
        return -1;
    }
    
    public static void intercanvia(int[] vector) {
        
        if (vector.length < 2) {
            return;
        }
        // 0 -> primera posición
        // length - 1 -> última posición
            
        int aux = vector[0];
        vector[0] = vector[vector.length-1];
        vector[vector.length-1] = aux;

    }
}
