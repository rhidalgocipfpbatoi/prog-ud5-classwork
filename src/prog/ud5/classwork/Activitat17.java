/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud5.classwork;

/**
 *
 * @author batoi
 */
public class Activitat17 {
    
    //Constantes para mejorar legibilidad del código
    static final int NOMBRE = 0;
    static final int APELLIDOS = 1;
    static final int EDAD = 2;
    static final int CICLO = 3;
    static final int CURSO = 4;
    
    // Ciclos disponibles
    static final String[] CICLOS = {"DAM", "DAW", "ASIX"};
    
    static final String[] CABECERA = {"Nombre", "Apellidos", "Edad", "Ciclo", "Curso"};
    
    public static void main(String[] args) {
        
        String[][] matriz = crearMatriz();
        
        System.out.println("Punto 1");
        visualizarMatrizAlumnos(matriz);
        
        //Punto 2
        System.out.println("Punto 2");
        visualizarCabecera();
        visualizarPorCiclo(matriz, CICLOS[1]);
        visualizarPorCiclo(matriz, CICLOS[2]);
        System.out.println("");
        
        //Punto 3
        System.out.println("Punto 3");
        System.out.println("Edad media: " + obtenerMediaEdad(matriz));
        System.out.println("");
        
        //Punto 4
        System.out.println("Punto 4");
        promocionarAlumnosACurso(matriz, 2);
        visualizarMatrizAlumnos(matriz);
        
        //Punto 5
        System.out.println("Punto 5");
        visualizarMatriz(crearTablaTotalMatriculados(matriz));
    }

    public static String[][] crearMatriz() {
        String[][]matriz = new String[5][5];
        
        rellenarFila(matriz,0, "Joan", "Pérez Aura", "24", CICLOS[2], "1");
        rellenarFila(matriz, 1, "María", "Sánchez García", "18", CICLOS[1], "1");
        rellenarFila(matriz, 2, "Pepa", "Egea Juan", "21", CICLOS[0], "1");
        rellenarFila(matriz,3, "Ana María", "Hernández Julián", "20", CICLOS[1], "2");
        rellenarFila(matriz,4, "Francesc", "Juan Juan", "28", CICLOS[1], "1");
        
        return matriz;
    }

    public static void rellenarFila(
            String[][] matriz, int fila,
            String nombre, String apellidos,
            String edad, String ciclo, String curso) {

        matriz[fila][NOMBRE] = nombre;
        matriz[fila][APELLIDOS] = apellidos;
        matriz[fila][EDAD] = edad;
        matriz[fila][CICLO] = ciclo;
        matriz[fila][CURSO] = curso;
    }
    
    public static void visualizarMatrizAlumnos(String[][] matrizAlumnos) {
        visualizarCabecera();
        visualizarMatriz(matrizAlumnos);
    }
    
    public static void visualizarCabecera() {
        visualizarFila(CABECERA);
    }

    public static void visualizarMatriz(String[][]matriz) {
        for (String[] fila : matriz) {
            visualizarFila(fila);
        }
        System.out.println("");
    }
    
    public static void visualizarFila(String[] fila) {
        for (String celda : fila) {
            visualizarCelda(celda);
        }
        System.out.println("");
    }
    
    public static void visualizarCelda(String celda) {
        System.out.printf("%17s", celda + "\t");
    }

    public static void visualizarPorCiclo(String[][]matriz, String ciclo) {
         for (String[] fila : matriz) {
             if (fila[CICLO].equals(ciclo)) {
                 visualizarFila(fila);
             }
         }
    }

    public static int obtenerMediaEdad(String[][]matriz) {
        int suma = 0;
        for (String[] fila : matriz) {
             suma += Integer.parseInt(fila[EDAD]);
         }

        return suma / matriz.length;
    }

    public static void promocionarAlumnosACurso(String[][]matriz, int cursoASerPromocionado) {
        for (String[] fila : matriz) {
            if (fila[CURSO].equals(String.valueOf(cursoASerPromocionado-1))) {
                fila[CURSO] = String.valueOf(cursoASerPromocionado);
            }
        }
    }

    public static String[][] crearTablaTotalMatriculados(String [][]matriz) {
        
        String[][] totalMatriculados = new String[CICLOS.length][2];
        
        for (int i = 0; i < CICLOS.length; i++) {
            totalMatriculados[i][0] = CICLOS[i];
            totalMatriculados[i][1] = String.valueOf(contarAlumnosEnCiclo(matriz, totalMatriculados[i][0]));
        }

        return totalMatriculados;
    }

    public static int contarAlumnosEnCiclo(String[][] matriz, String ciclo) {
        int contador = 0;
        for (String[] fila : matriz) {
            if (fila[CICLO].equals(ciclo)) {
                contador++;
            }
        }

        return contador;
    }
}
